package com.example.exerc01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button cmdNouvelEmploye = (Button) findViewById(R.id.idButtonNouvelEmploye);
        Button cmdListeEmploye = (Button) findViewById(R.id.idButtonListeEmploye);

        cmdNouvelEmploye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityNouvelEmploye.class);
                startActivity(intent);
            }
        });
        cmdListeEmploye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MainActivity.this, ActivityListeEmploye.class);
                startActivity(intent);
            }
        });
    }
}