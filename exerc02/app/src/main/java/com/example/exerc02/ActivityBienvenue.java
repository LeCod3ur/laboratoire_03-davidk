package com.example.exerc02;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityBienvenue extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bienvenue);

        Intent intent = getIntent();
        String nom = intent.getStringExtra ("lastName");
        String prenom = intent.getStringExtra ("firstName");
        TextView textView = (TextView) findViewById(R.id.idTextViewBienvenue);
        String message = getString(R.string.text_bienvenue) + " \n" + nom + " " + prenom;
        textView.setText(message);
    }
}
