package com.example.exerc02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.app.*;
import android.view.*;

public class MainActivity extends AppCompatActivity {
    Button btnAfficher;
    EditText txtNom;
    EditText txtPrenom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        btnAfficher = (Button) findViewById(R.id.idButtonAfficher);
        btnAfficher.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                txtNom = (EditText) findViewById(R.id.idEditTextNom);
                txtPrenom = (EditText) findViewById(R.id.idEditTextPrenom);

                Intent intent = new Intent(MainActivity.this, ActivityBienvenue.class);
                intent.putExtra("lastName", txtNom.getText().toString());
                intent.putExtra("firstName", txtPrenom.getText().toString());
                startActivity(intent);
            }
        });
    }
}