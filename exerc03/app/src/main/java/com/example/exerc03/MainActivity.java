package com.example.exerc03;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    Button btnAfficher;
    EditText txtMatricule;
    EditText txtNom;
    EditText txtPrenom;
    RadioGroup radioSexe;
    RadioGroup radioEtatCivil;
    CheckBox checkBoxFrancais, checkBoxAnglais, checkBoxAutre;
    EditText txtAutreLangue;
    EditText txtSalaire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnAfficher = (Button) findViewById(R.id.idButtonAfficher);
        btnAfficher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMatricule = (EditText) findViewById(R.id.idEditTextMatricule);
                txtNom = (EditText) findViewById(R.id.idEditTextNom);
                txtPrenom = (EditText) findViewById(R.id.idEditTextPrenom);
                txtSalaire = (EditText) findViewById(R.id.idTextSalaire);

                radioSexe = (RadioGroup) findViewById(R.id.RadioGroupSexe);
                String selectedRadioValueSexe = ((RadioButton) findViewById(radioSexe.getCheckedRadioButtonId())).getText().toString();

                radioEtatCivil = (RadioGroup) findViewById(R.id.idRadioButtonEtatCivil);
                String selectedRadioValueEtatCivil = ((RadioButton) findViewById(radioEtatCivil.getCheckedRadioButtonId())).getText().toString();

                checkBoxFrancais = (CheckBox) findViewById(R.id.idLangueFrancais);
                checkBoxAnglais = (CheckBox) findViewById(R.id.idLangueAnglais);
                checkBoxAutre = (CheckBox) findViewById(R.id.idLangueAutre);
                txtAutreLangue = (EditText) findViewById(R.id.idTextAutreLangue);

                String checkBoxLangueSelected = "";
                if (checkBoxFrancais.isChecked()){
                    checkBoxLangueSelected += checkBoxFrancais.getText().toString();
                } if  (checkBoxAnglais.isChecked()) {
                    checkBoxLangueSelected +=  ", " + checkBoxAnglais.getText().toString();
                } if (checkBoxAutre.isChecked()) {
                    checkBoxLangueSelected += ", " + txtAutreLangue.getText().toString();
                }

                Intent intent = new Intent(MainActivity.this, ActivityEmploye.class);
                intent.putExtra("Matricule", (txtMatricule.getText().toString()));
                intent.putExtra("Nom", (txtNom.getText().toString()));
                intent.putExtra("Prenom", (txtPrenom.getText().toString()));
                intent.putExtra("Salaire", txtSalaire.getText().toString());
                intent.putExtra("RadioGroupSexe", selectedRadioValueSexe);
                intent.putExtra("RadioGroupEtatCivil", selectedRadioValueEtatCivil);
                intent.putExtra("Langue", checkBoxLangueSelected);
                startActivity(intent);
            }
        });
    }
}