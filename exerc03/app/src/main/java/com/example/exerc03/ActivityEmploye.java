package com.example.exerc03;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

public class ActivityEmploye extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employe);

        Intent intent = getIntent();
        String matricule = intent.getStringExtra("Matricule");
        String nom = intent.getStringExtra("Nom");
        String prenom = intent.getStringExtra("Prenom");
        String montant = intent.getStringExtra("Salaire");
        String sexe = intent.getStringExtra("RadioGroupSexe");
        String etatCivil = intent.getStringExtra("RadioGroupEtatCivil");
        String langue = intent.getStringExtra("Langue");

        TextView textViewMatricule = (TextView) findViewById(R.id.idTextViewMatricule);
        textViewMatricule.setText(matricule);

        TextView textViewNom = (TextView) findViewById(R.id.idTextViewNom);
        String nomComplet = prenom + ", " + nom;
        textViewNom.setText(nomComplet);

        TextView textViewSexe = (TextView) findViewById(R.id.idTextViewSexe);
        textViewSexe.setText(sexe);

        TextView textViewEtatCivil = (TextView) findViewById(R.id.idTextViewEtatCivil);
        textViewEtatCivil.setText(etatCivil);

        TextView textViewLangue = (TextView) findViewById(R.id.idTextViewLangue);
        textViewLangue.setText(langue);

        TextView textViewSalaire = (TextView) findViewById(R.id.idTextViewSalaire);
        String salaire = montant + "$.";
        textViewSalaire.setText(salaire);

    }
}
