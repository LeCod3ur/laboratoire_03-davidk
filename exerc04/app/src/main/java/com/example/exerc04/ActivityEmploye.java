package com.example.exerc04;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ActivityEmploye extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employe);

        Intent intent = getIntent();
        Employe employe = (Employe)intent.getSerializableExtra("EmployeObj");

        String matricule = employe.getMatricule();
        TextView textViewMatricule = (TextView) findViewById(R.id.idTextViewMatricule);
        textViewMatricule.setText(matricule);

        String nomComplet = employe.getPrenom() + ", " + employe.getNom();
        TextView textViewNom = (TextView) findViewById(R.id.idTextViewNom);
        textViewNom.setText(nomComplet);

        String sexe = employe.getSexe();
        TextView textViewSexe = (TextView) findViewById(R.id.idTextViewSexe);
        textViewSexe.setText(sexe);

        String etatCivil = employe.getEtatCivil();
        TextView textViewEtatCivil = (TextView) findViewById(R.id.idTextViewEtatCivil);
        textViewEtatCivil.setText(etatCivil);

        String langue = employe.getLangue();
        TextView textViewLangue = (TextView) findViewById(R.id.idTextViewLangue);
        textViewLangue.setText(langue);

        String salaire = employe.getSalaire() + "$.";
        TextView textViewSalaire = (TextView) findViewById(R.id.idTextViewSalaire);
        textViewSalaire.setText(salaire);
    }

}

